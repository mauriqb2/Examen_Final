package com.ucbcba.proyecto.proyecto.Controllers;

import com.ucbcba.proyecto.proyecto.Entities.Pais;
import com.ucbcba.proyecto.proyecto.Services.PaisService;
import com.ucbcba.proyecto.proyecto.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PaisController {

    private PaisService paisService;
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService){
        this.userService=userService;
    }

    @Autowired
    private void setPaisService(PaisService paisService){
        this.paisService=paisService;
    }

    @RequestMapping(value = "/admin/pais", method = RequestMethod.GET)
    public String Nuevo(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("user",userService.findByEmail(name));
        model.addAttribute("pais",new Pais());
        return "PaisForm";
    }
    @RequestMapping(value = "/admin/pais/new", method = RequestMethod.POST)
    public String save(@Valid Pais pais, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName();
            model.addAttribute("user",userService.findByEmail(name));
            return "PaisForm";
        }
        paisService.savePais(pais);
        return "redirect:/bienvenidos";
    }
}
